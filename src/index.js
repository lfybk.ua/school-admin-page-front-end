import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './component/app';

import { AuthContextProvider } from "./context"


ReactDOM.render(
  <React.StrictMode>
    <AuthContextProvider>
      <App />
    </AuthContextProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
