import { createContext, useState } from "react"

export const AuthContext = createContext({});

export function AuthContextProvider({ children }) {
  const [auth, setAuth] = useState({})



  const isAuth = () => {
    return auth.key && auth.fullName && auth.phone && auth.role;
  }

  const setAuthData = (data) => {
    setAuth(data);

  }

  const deleteAuthData = () => {
    setAuth({})
  }

  return (
    <AuthContext.Provider
      value={{
        auth,
        isAuth,
        deleteAuthData,
        setAuthData
      }}>
      {children}
    </AuthContext.Provider>
  )
}