import React from "react";

import "./style.scss";


const Header = () => {
  return (
    <header className="header">
      <img src="/image/logo.png" alt="logo" className="header__logo" />
      <h1 className="header__title" >Комунальний заклад освіти "НВК № 4 "СЗШ-ДНЗ (ДС)" ДМР</h1>
    </header>
  )
}

export default Header;