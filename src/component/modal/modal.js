import "./style.scss";

const Modal = ({ Component, data, send, close, defaultValue }) => {
  return (
    <div className="modal">
      <div className="modal__box">
        <div className="modal__menu">
          <button onClick={() => close()} className="modal__btn modal__btn-close">×</button>
        </div>
        <Component defaultValue={defaultValue} send={send} data={data} />
      </div>
    </div>
  )
}

export default Modal;