import React from "react";
import "./style.scss"

import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

import Header from "../header";
import Footer from "../footer";
import Toggler from "../toggler";

import Teacher from "../../page/teacher";
import Admin from "../../page/admin";
import Main from "../../page/main";

const App = () => {

  return (
    <BrowserRouter>
      <Header />
      <main className="main">
        <Toggler />
        <Routes>
          <Route path="/" element={<Main />} />
          <Route path="teacher" element={<Teacher />} />
          <Route path="admin" element={<Admin />} />
        </Routes>
      </main>
      <Footer />
    </BrowserRouter>
  )
}

export default App;