import { useEffect, useRef, useState } from "react";

import mark from "../../../api/mark";


import List from "../../list";
import Modal from "../../modal";

const ModalBody = ({ data, send, defaultValue = {} }) => {
  const [errors, setErrors] = useState([]);

  const mark = useRef(null)
  const text = useRef(null)
  const date = useRef(null)
  const idSubject = useRef(null)

  useEffect(() => {
    mark.current.value = defaultValue.mark || "";
    text.current.value = defaultValue.text || "";
    date.current.value = defaultValue.date ? defaultValue.date.slice(0, 10) : "";
    idSubject.current.value = defaultValue.idSubject || "";
  }, [])

  const validator = (e) => {
    e.preventDefault()

    const errorList = [];

    const markValue = mark.current.value;
    const textValue = text.current.value;
    const dateValue = date.current.value;
    const idSubjectValue = idSubject.current.value;

    if (!(markValue || textValue)) {
      errorList.push('Потрібно ввести хоча б  "Повідомлення" або "Оцінку"')
    }

    if (markValue && (markValue > 12 || markValue < 0)) {
      errorList.push('Оцінка має бути 0>= та <=12')
    }

    if (textValue && textValue.length > 50) {
      errorList.push('Оцінка має бути 0>= та <=12')
    }

    if (idSubjectValue === "0") {
      errorList.push('Оберіть предмет')
    }

    if (errorList[0]) {
      setErrors(errorList)
      return;
    }

    send({
      mark: markValue || 0,
      text: textValue || null,
      idSubject: idSubjectValue,
      date: dateValue ? new Date(dateValue) : new Date()
    })

    setErrors([])
  }

  return (
    <form className="modal-body__form">
      <input ref={date} type="date" />
      <select ref={idSubject} >
        <option value="0">Оберіть предмет</option>
        {data.subject.map((e, i) => {
          return <option key={i + "mark"} value={e.id}>{e.subjectName}</option>
        })}
      </select>
      <div className="modal-body__box">
        <label htmlFor="text">Повідомлення:</label>
        <input ref={text} id="text" type="text" maxLength="50" />
      </div>
      <div className="modal-body__box">
        <label htmlFor="mark">Оцінка:</label>
        <input ref={mark} id="mark" type="number" max="12" min="0" />
      </div>
      <input onClick={validator} className="btn" type="submit" value="зберегти" />
      <ul>
        {errors.map((e, i) => {
          return <li key={i + "errorList"}>{e}</li>
        })}
      </ul>
    </form>
  )
}

const Mark = ({ token, id }) => {
  const [data, setData] = useState({})
  const [state, setState] = useState({ eventType: null })

  useEffect(() => {
    const fetchData = async () => {
      setData(await mark.getMarkStudent(id, token))
    }

    fetchData()
  }, [id])

  const close = () => setState({ eventType: null });
  const add = () => setState({ eventType: "ADD" });
  const edit = (e) => setState({ eventType: "EDIT", id: e.id });

  const send = async (obj) => {
    if (state.eventType === "ADD") {
      obj.idStudent = id;
      await mark.addMark(token, obj);
    }
    if (state.eventType === "EDIT") {
      obj.id = state.id;

      await mark.editMark(token, obj);
    }
    setData(await mark.getMarkStudent(id, token))
    setState({ eventType: null })
  }

  const del = async (e) => {
    await mark.deleteMark(e.id, token)

    setData(await mark.getMarkStudent(id, token))
  };

  return (
    <>
      {data.mark ? <List data={data.mark}
        Component={({ e }) => {
          const { subjectName } = data.subject.filter(({ id }) => e.idSubject === id)[0]

          return (
            <p>
              <span>{e.date.slice(0, 10)}</span>: {subjectName} {e.mark ? `(${e.mark}) ` : ""} {e.text ? "- " + e.text : ""}
            </p>
          )
        }}
        add={add} edit={edit} del={del} /> : ""}
      {state.eventType ? <Modal defaultValue={data.mark.filter((e) => e.id === state.id)[0]} close={close} Component={ModalBody} data={data} send={send} /> : ""}
    </>
  )
}


export default Mark;