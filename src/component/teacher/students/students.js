import { useEffect, useState } from "react";

import mark from "../../../api/mark";

const Students = ({ token, name, setStudent }) => {
  const [students, setStudents] = useState([])

  useEffect(() => {
    const fetchData = async () => {
      setStudents(await mark.getClassStudent(name, token))
    }

    fetchData()

  }, [name])

  return (
    <div>
      <select onChange={e => {
        if (!e.target.value) return;
        setStudent(students.filter(({ id }) => e.target.value === id + "")[0]);
      }}>
        <option value="">Оберіть учня</option>
        {
          students.map((e, i) => {
            return <option key={i + "students"} value={e.id}>{e.fullName}</option>
          })
        }
      </select>
    </div>
  )
}


export default Students;
