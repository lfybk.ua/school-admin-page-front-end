import { useEffect, useState } from "react";

import mark from "../../../api/mark";

const Class = ({ token, setClassName }) => {
  const [names, setNames] = useState([])

  useEffect(() => {
    const fetchData = async () => {
      const classNames = await mark.getClassName(token);
      const className = []

      for (let item of classNames) {
        if (className.indexOf(item.className) === -1) {
          className.push(item.className)
        }
      }

      setNames(className);
    }

    fetchData()
  }, [])

  return (
    <select onChange={(e) => setClassName(e.target.value)}>
      <option value="">Оберіть клас</option>
      {names.map((e, i) => {
        return <option value={e} key={i + "className"}>{e}</option>
      })}
    </select>
  )
}


export default Class;