import "./style.scss";

const List = ({ data, Component, add, edit, del }) => {
  return (
    <div className="list">
      <div className="list__box">
        <ul className="list__ul">
          {data.map((e, id) => {
            return (
              <li className="list__item" key={id + "mark"}>
                <Component e={e} />
                <div className="list__menu">
                  {del ? <button onClick={() => del(e)} className="list__btn list__btn-del">DEL</button> : ""}
                  {edit ? <button onClick={() => edit(e)} className="list__btn list__btn-edit">EDIT</button> : ""}
                </div>
              </li>
            )
          })}
        </ul>
        {add ? <button onClick={() => add()} className="list__btn list__btn-add">ADD</button> : ""}
      </div>
    </div>
  )
}


export default List;