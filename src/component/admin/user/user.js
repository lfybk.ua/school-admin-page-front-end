import { useEffect, useRef, useState } from "react";
import List from "../../list";
import Modal from "../../modal";

import user from "../../../api/user";

const ModalBody = ({ data, send, defaultValue = {} }) => {
  const [errors, setErrors] = useState([]);

  const role = useRef(null)
  const password = useRef(null)
  const phone = useRef(null)
  const fullName = useRef(null)
  const email = useRef(null)

  useEffect(() => {
    role.current.value = defaultValue.role || "student";
    phone.current.value = defaultValue.phone || "";
    fullName.current.value = defaultValue.fullName || "";
  }, [])

  const validator = (e) => {
    e.preventDefault()

    const errorList = [];

    const roleValue = role.current.value;
    const passwordValue = password.current.value;
    const phoneValue = phone.current.value;
    const fullNameValue = fullName.current.value;
    const emailValue = email.current.value;


    if (passwordValue && (passwordValue.length >= 20 || passwordValue.length <= 10)) {
      errorList.push('Password має бути не >=10, та не <=20')
    }

    if (phoneValue && !/^[0-9]{10}$/.test(phoneValue)) {
      errorList.push('Номер має бути 0732309234')
    }

    if (fullNameValue && (fullNameValue.length >= 40 || fullNameValue.length <= 5)) {
      errorList.push('Ім\'я має бути не >=5, та не <=40')
    }

    if (emailValue && !/^(([a-z]{5,30})|([a-z]{1,30}\.[a-z]{1,30}))@[a-z]{1,10}\.[a-z]{1,10}$/.test(emailValue)) {
      errorList.push('Email має бути qweasdqwe.qwe@qwe.qwe')
    }

    if (data.filter(({ email }) => email === emailValue)[0]) {
      errorList.push('Email вже використовується')
    }

    if (errorList[0]) {
      setErrors(errorList)
      return;
    }

    send({
      role: roleValue,
      password: passwordValue,
      phone: phoneValue,
      fullName: fullNameValue,
      email: emailValue,
    })

    setErrors([])
  }

  return (
    <form className="modal-body__form">
      <select ref={role}>
        <option value="student">student</option>
        <option value="admin">admin</option>
        <option value="teacher">teacher</option>
      </select>
      <div className="modal-body__box">
        <label htmlFor="password">Password:</label>
        <input ref={password} id="password" type="text" maxLength="20" minLength="10" />
      </div>
      <div className="modal-body__box">
        <label htmlFor="phone">Phone:</label>
        <input ref={phone} id="phone" type="text" maxLength="10" minLength="10" placeholder="0123456789" />
      </div>
      <div className="modal-body__box">
        <label htmlFor="fullName">Повне ім'я:</label>
        <input ref={fullName} id="fullName" type="text" maxLength="40" minLength="5" />
      </div>
      <div className="modal-body__box">
        <label htmlFor="email">Email:</label>
        <input ref={email} id="email" type="text" maxLength="40" minLength="5" />
      </div>
      <input onClick={validator} className="btn" type="submit" value="зберегти" />
      <ul>
        {errors.map((e, i) => {
          return <li key={i + "errorListUser"}>{e}</li>
        })}
      </ul>
    </form>
  )
}

const User = ({ token }) => {
  const [data, setData] = useState([])
  const [state, setState] = useState({ eventType: null })

  useEffect(() => {
    const fetchData = async () => {
      setData(await user.getUsers(token));
    }

    fetchData()
  }, [])


  const close = () => setState({ eventType: null });
  const add = async () => setState({ eventType: "ADD" });
  const edit = async (e) => setState({ eventType: "EDIT", id: e.id });

  const del = async (e) => {
    await user.deleteUsers(e.id, token)

    setData(await user.getUsers(token))
  }

  const send = async (obj) => {
    if (state.eventType === "ADD") {
      await user.addUsers(token, obj);
    }
    if (state.eventType === "EDIT") {
      obj.id = state.id;

      await user.editUsers(token, obj);
    }
    setData(await user.getUsers(token))
    setState({ eventType: null })
  }

  return (
    <div>
      <List data={data}
        Component={({ e }) => {
          return (
            <p>
              {e.fullName} - email: {e.email} | phone: {e.phone} | role: {e.role}
            </p>
          )
        }}
        add={add} edit={edit} del={del} />
      {state.eventType ? <Modal defaultValue={data.filter((e) => e.id === state.id)[0]} close={close} Component={ModalBody} data={data} send={send} /> : ""}
    </div>
  )
}


export default User;