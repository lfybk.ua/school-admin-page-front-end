import { useState, useEffect } from "react";

import List from "../../list";

import feedback from "../../../api/feedback";


const Feedback = ({ token }) => {

  const [data, setData] = useState([])

  useEffect(() => {
    const fetchData = async () => {
      setData(await feedback.getFeedback(token));
    }

    fetchData()
  }, [])

  console.log(data);

  const del = async (e) => {
    await feedback.deleteFeedback(token, e.id)

    setData(await feedback.getFeedback(token));
  }

  return (
    <div>
      <List data={data}
        Component={({ e }) => {
          return (
            <p>
              {e.fullName} - email: {e.email} | phone: {e.phone}
            </p>
          )
        }}
        add={null} edit={null} del={del} />
    </div>
  )
}

export default Feedback;