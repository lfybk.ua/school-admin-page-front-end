import { useEffect, useRef, useState } from "react";
import List from "../../list";
import Modal from "../../modal";

import calendar from "../../../api/calendar";

const getDateStr = (date) => {
  let str = "";
  str += date.getFullYear();
  str += (date.getMonth() + 1) / 10 <= 1 ? "-0" + (date.getMonth() + 1) : "" + (date.getMonth() + 1);
  str += date.getDate() / 10 <= 1 ? "-0" + date.getDate() : "-" + date.getDate();

  return str;
}

const ModalBody = ({ send, defaultValue = {} }) => {
  const [errors, setErrors] = useState([]);

  const date = useRef(null)
  const event = useRef(null)

  useEffect(() => {
    let toDay = getDateStr(new Date());

    date.current.value = defaultValue?.date?.slice(0, 10) || toDay;
    event.current.value = defaultValue.event || "";
  }, [])

  const validator = (e) => {
    e.preventDefault()

    const errorList = [];

    const dateValue = date.current.value;
    const eventValue = event.current.value;


    if (!dateValue) {
      errorList.push('Оберіть дату')
    }

    if (eventValue && (eventValue.length >= 50 || eventValue.length <= 5)) {
      errorList.push('Date має бути не >=10, та не <=20')
    }

    if (errorList[0]) {
      setErrors(errorList)
      return;
    }

    send({
      date: getDateStr(new Date(new Date(dateValue).getTime() + 86400000)),
      event: eventValue,
    })

    setErrors([])
  }

  return (
    <form>
      <div className="modal-body__box">
        <label htmlFor="date">Дата:</label>
        <input ref={date} id="date" type="date" />
      </div>
      <div className="modal-body__box">
        <label htmlFor="event">Текст:</label>
        <input ref={event} id="event" type="text" maxLength="50" minLength="5" />
      </div>
      <input onClick={validator} className="btn" type="submit" value="зберегти" />
      <ul>
        {errors.map((e, i) => {
          return <li key={i + "errorListUser"}>{e}</li>
        })}
      </ul>
    </form>
  )
}

const Calendar = ({ token }) => {
  const [data, setData] = useState([])
  const [state, setState] = useState({ eventType: null })

  useEffect(() => {
    const fetchData = async () => {
      setData(await calendar.getCalendar());
    }

    fetchData()
  }, [])

  const add = () => setState({ eventType: "ADD" });
  const edit = (e) => setState({ eventType: "EDIT", id: e.id });

  const del = async (e) => {
    await calendar.deleteCalendar(e.id, token)

    setData(await calendar.getCalendar())
  }

  const close = () => setState({ eventType: null });

  const send = async (obj) => {
    if (state.eventType === "ADD") {
      await calendar.addCalendar(token, obj);
    }
    if (state.eventType === "EDIT") {
      obj.id = state.id;

      await calendar.editCalendar(token, obj);
    }

    setData(await calendar.getCalendar())
    setState({ eventType: null })
  }

  return (
    <div>
      <List data={data}
        Component={({ e }) => {
          return (
            <p>
              {e.date.slice(0, 10)} - {e.event}
            </p>
          )
        }}
        add={add} edit={edit} del={del} />
      {state.eventType ? <Modal defaultValue={data.filter((e) => e.id === state.id)[0]} close={close} Component={ModalBody} data={data} send={send} /> : ""}
    </div>
  )
}


export default Calendar;