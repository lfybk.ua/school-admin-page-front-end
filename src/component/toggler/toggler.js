import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../../context"

import "./style.scss";

const Toggler = () => {
  const { auth } = useContext(AuthContext)
  const navigate = useNavigate()
  console.log(auth);

  return (
    <div className="toggler">
      <button onClick={() => navigate("admin")} disabled={auth.role !== "admin"} className="toggler__btn">Панель адміністратора</button>
      <button onClick={() => navigate("teacher")} disabled={auth.role !== "teacher" && auth.role !== "admin"} className="toggler__btn">Панель вчителя</button>
    </div>
  )
}

export default Toggler;