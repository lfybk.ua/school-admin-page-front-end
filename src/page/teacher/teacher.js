import { useContext, useState, useEffect } from "react";
import { Class, Students, Mark } from "../../component/teacher";
import { AuthContext } from "../../context";
import { useNavigate } from "react-router-dom";

import "./style.scss";

const Teacher = () => {
  const { auth } = useContext(AuthContext)
  const [data, setDate] = useState({})
  const navigate = useNavigate();

  useEffect(() => {
    if (!auth.key) {
      navigate("/")
    }
  }, [])

  const setClassName = (name) => {
    if (!name) return;
    setDate({ className: name });
  }

  const setStudent = (student) => {
    setDate({ className: data.className, student: student })
  }

  console.log(data);

  return (
    <section>
      <Class token={auth.key} setClassName={setClassName} />
      {data.className ? <Students token={auth.key} name={data.className} setStudent={setStudent} /> : ""}
      {data.student ? <div className="teacher__text"> <p>email: {data.student.email}, tel: {data.student.phone}</p></div> : ""}
      {data.student ? <Mark token={auth.key} id={data.student.id} /> : ""}
    </section>
  )
}
//class
export default Teacher;