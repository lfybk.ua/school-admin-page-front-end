import "./style.scss"

import user from "../../api/user"
import { useContext, useEffect, useRef } from "react"

import { AuthContext } from "../../context/auth-context"

const Main = () => {
  const { setAuthData } = useContext(AuthContext)

  const input = useRef(null)

  const send = async (e) => {
    e.preventDefault()
    const token = input.current.value.trim();

    if (!token) return;

    const dataUser = await user.admin(token)

    console.log(dataUser);
    setAuthData(dataUser)

  }

  return (
    <section className="main">
      <form>
        <input ref={input} type="text" />
        <button className="btn" onClick={send}>Вхід</button>
      </form>
    </section>
  )
}

export default Main;