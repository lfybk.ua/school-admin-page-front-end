import { useContext, useState } from "react";
import { AuthContext } from "../../context";

import { User, Calendar, Feedback } from "../../component/admin";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

const Admin = () => {
  const { auth } = useContext(AuthContext)
  const navigate = useNavigate();
  const [state, setState] = useState(0)

  const components = [
    <User token={auth.key} />,
    <Calendar token={auth.key} />,
    <Feedback token={auth.key} />
  ]

  useEffect(() => {
    if (!auth.key) {
      navigate("/")
    }
  }, [])

  return (
    <section>
      <select onChange={(e) => {
        setState(e.target.value || [])
      }}>
        <option value="0">user</option>
        <option value="1">calendar</option>
        <option value="2">feedback</option>
      </select>
      {components[state]}

    </section>
  )
}

export default Admin;