import url from "./url"
import inquiry from "./inquiry";

const mark = {
  async getClassName(token) {
    return await inquiry.get({
      url: url.class,
      headers: {
        authorization: token
      }
    })
  },
  async getClassStudent(id, token) {
    return await inquiry.get({
      url: url.students,
      param: `/${id}`,
      headers: {
        authorization: token
      }
    })
  },
  async getMarkStudent(id, token) {
    return await inquiry.get({
      url: url.student,
      param: `/${id}`,
      headers: {
        authorization: token
      }
    })
  },
  async addMark(token, body) {
    return await inquiry.post({
      url: url.addMark,
      headers: {
        authorization: token
      },
      sendData: body
    })
  },
  async editMark(token, body) {
    return await inquiry.put({
      url: url.editMark,
      headers: {
        authorization: token
      },
      sendData: body
    })
  },
  async deleteMark(id, token) {
    return await inquiry.delete({
      url: url.deleteMark,
      param: `/${id}`,
      headers: {
        authorization: token
      }
    })
  },
}



export default mark;