import url from "./url"
import inquiry from "./inquiry";

const calendar = {
  async getCalendar(token) {
    return await inquiry.get({
      url: url.getCalendar,
      headers: {
        authorization: token
      }
    })
  },
  async addCalendar(token, data) {
    return await inquiry.post({
      url: url.addCalendar,
      headers: {
        authorization: token
      },
      sendData: data
    })
  },
  async editCalendar(token, data) {
    return await inquiry.put({
      url: url.editCalendar,
      headers: {
        authorization: token
      },
      sendData: data
    })
  },
  async deleteCalendar(id, token) {
    return await inquiry.delete({
      url: url.deleteCalendar,
      headers: {
        authorization: token
      },
      param: `/${id}`
    })
  },
}

export default calendar;