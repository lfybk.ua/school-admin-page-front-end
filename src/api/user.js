import url from "./url"
import inquiry from "./inquiry";

const user = {
  async getUsers(token) {
    return await inquiry.get({
      url: url.getUsers,
      headers: {
        authorization: token
      }
    })
  },
  async addUsers(token, data) {
    return await inquiry.post({
      url: url.addUsers,
      headers: {
        authorization: token
      },
      sendData: data
    })
  },
  async editUsers(token, data) {
    return await inquiry.put({
      url: url.editUsers,
      headers: {
        authorization: token
      },
      sendData: data
    })
  },
  async deleteUsers(id, token) {
    return await inquiry.delete({
      url: url.deleteUsers,
      headers: {
        authorization: token
      },
      param: `/${id}`
    })
  },
  async admin(token) {
    return await inquiry.get({
      url: url.admin,
      headers: {
        authorization: token
      },
    })
  },
}

export default user;