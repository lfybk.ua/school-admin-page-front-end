import { settings } from "../settings";
const dev = settings.dev;
const mainURL = dev ? "http://localhost:5050/api" : "https://school123ff.herokuapp.com/api";

const url = {
  class: `${mainURL}/diary/name`,
  students: `${mainURL}/diary/students`,
  student: `${mainURL}/diary/mark/student`,
  addMark: `${mainURL}/diary/mark`,
  editMark: `${mainURL}/diary/mark/edit`,
  deleteMark: `${mainURL}/diary/delete`,
  getUsers: `${mainURL}/auth/users`,
  addUsers: `${mainURL}/auth/add`,
  editUsers: `${mainURL}/auth/edit`,
  deleteUsers: `${mainURL}/auth/delete`,
  admin: `${mainURL}/auth/admin`,
  getCalendar: `${mainURL}/calendar`,
  addCalendar: `${mainURL}/calendar/add`,
  editCalendar: `${mainURL}/calendar/edit`,
  deleteCalendar: `${mainURL}/calendar/delete`,
  getFeedback: `${mainURL}/feedback/`,
  deleteFeedback: `${mainURL}/feedback/delete`,
}


export default url;