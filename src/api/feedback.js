import url from "./url"
import inquiry from "./inquiry";

const feedback = {
  async getFeedback(token) {
    return await inquiry.get({
      url: url.getFeedback,
      headers: {
        authorization: token
      }
    })
  },
  async deleteFeedback(token, id) {
    return await inquiry.delete({
      url: url.deleteFeedback,
      headers: {
        authorization: token
      },
      param: `/${id}`
    })
  },
}

export default feedback;